//
//  Copyright (C) 2012 Tom Beckmann, Rico Tzschichholz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Clutter;
using Meta;
using Gala;

namespace OdinSwitcher {

    public class Main : Gala.Plugin {

        Gala.WindowManager? wm = null;
        WindowSwitcher? winswitcher = null;
        public override void initialize (Gala.WindowManager wm)
        {

            cl_initialize ();

            this.wm = wm;
            message ("Initializing Odin Switcher");
            winswitcher = new WindowSwitcher (wm);
            wm.ui_group.add_child (winswitcher);
            Meta.KeyBinding.set_custom_handler ("switch-applications", (Meta.KeyHandlerFunc) winswitcher.handle_switch_windows);
            Meta.KeyBinding.set_custom_handler ("switch-applications-backward", (Meta.KeyHandlerFunc) winswitcher.handle_switch_windows);
            Meta.KeyBinding.set_custom_handler ("switch-windows", (Meta.KeyHandlerFunc) winswitcher.handle_switch_windows);
            Meta.KeyBinding.set_custom_handler ("switch-windows-backward", (Meta.KeyHandlerFunc) winswitcher.handle_switch_windows);
            Meta.KeyBinding.set_custom_handler ("switch-group", (Meta.KeyHandlerFunc) winswitcher.handle_switch_windows);
            Meta.KeyBinding.set_custom_handler ("switch-group-backwards", (Meta.KeyHandlerFunc) winswitcher.handle_switch_windows);

        }

        public override void destroy () {
            winswitcher.destroy ();
        }
    }

    public class InternalUtils {
        public static int get_ui_scaling_factor () {
            return 1;
        }
    }
}

public Gala.PluginInfo register_plugin ()
{
    return Gala.PluginInfo () {
               name = "GalaOdinWindowSwitcher",
               author = "Gala Odin Window Switcher Authors",
               plugin_type = typeof (OdinSwitcher.Main),
               provides = Gala.PluginFunction.WINDOW_SWITCHER,
               load_priority = Gala.LoadPriority.IMMEDIATE
    };
}