
namespace OdinSwitcher {
    enum mutter_cl_t { mutter_cl_none, mutter_cl_default, mutter_cl_ecl,
                       mutter_cl_mkcl }

#if MUTTER_ECL
    const string ECL_APP = "ecl-mutter-main";
#endif

#if MUTTER_MKCL
    const string MKCL_APP = "mkcl-mutter-main";
#endif

    public void cl_initialize () {
#if MUTTER_ECL || MUTTER_MKCL
        mutter_cl_t mutter_cl;
        string mutter_cl_s = GLib.Environment.get_variable ("MUTTER_CL");
        if (mutter_cl_s == null)
            mutter_cl = mutter_cl_default;
        else if (mutter_cl_s.ascii_casecmp ("none") == 0)
            mutter_cl = mutter_cl_none;
        else if (mutter_cl_s.ascii_casecmp ("ecl") == 0)
            mutter_cl = mutter_cl_ecl;
        else if (mutter_cl_s.ascii_casecmp ("mkcl") == 0)
            mutter_cl = mutter_cl_mkcl;
        else {
            warning ("unknown value for env var MUTTER_CL: %s. Wanted one of ecl mkcl or none. Treating as none.\n", mutter_cl_s);
            mutter_cl = mutter_cl_none;
        }
#if MUTTER_ECL
        if (mutter_cl == mutter_cl_ecl || mutter_cl == mutter_cl_default)
            EclPlug.ecl_initialize (ECL_APP);
#endif
#if MUTTER_MKCL
        if (mutter_cl == mutter_cl_mkcl || mutter_cl == mutter_cl_default)
            MkclPlug.mkcl_initialize (MKCL_APP);
#endif
#endif
    }

}