#!/bin/bash
DATA=$(dirname "$BASH_SOURCE")
UNCRUSTIFY=$(command -v uncrustify)

if [ -z "$UNCRUSTIFY" ];
then
    echo "Uncrustify is not installed on your system."
    exit 1
fi

for DIR in "$DATA/../"{,Widgets}
do
    for FILE in $(find "$DIR" -name "*.vala")
    do
        "$UNCRUSTIFY" -c "$DATA/uncrustify.vala.cfg" --no-backup "$FILE"
   done
done
