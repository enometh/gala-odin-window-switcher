//
//  Copyright 2021 Aral Balkan <mail@ar.al>
//  Copyright 2020 Mark Story <mark@mark-story.com>
//  Copyright 2017 Popye <sailor3101@gmail.com>
//  Copyright (C) 2012 Tom Beckmann, Rico Tzschichholz
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Clutter;
using Meta;
using Gala;

namespace OdinSwitcher {

    public class WindowSwitcher : Clutter.Actor {
        const int MIN_DELTA = 100;
        const float BACKGROUND_OPACITY = 155.0f;
        const float DIM_WINDOW_BRIGHTNESS = -BACKGROUND_OPACITY / 255.0f;

        public WindowManager wm { get; construct; }

        WindowIcon? current_window = null;
        Meta.Window prev = null;

        Actor window_clones;
        List<unowned Actor> clone_sort_order;

        Clutter.Actor container;
        RoundedActor indicator;
        Clutter.Text caption;

        int ui_scale_factor = 1;
        FileMonitor monitor;

        Actor background;

        uint modifier_mask;
        int64 last_switch = 0;
        bool closing = false;
        ModalProxy modal_proxy;

        public WindowSwitcher (WindowManager wm) {
            Object (wm: wm);
        }

        construct {
            ui_scale_factor = InternalUtils.get_ui_scaling_factor ();

            window_clones = new Actor ();
            window_clones.actor_removed.connect (window_removed);

            background = new Actor ();
            background.background_color = Color.get_static (StaticColor.BLACK);
            update_background ();

            add_child (background);
            add_child (window_clones);
            create_components ();

            wm.get_display ().get_context ().get_backend ().get_monitor_manager ()
             .monitors_changed.connect (update_actors);
            visible = false;
            reactive = true;
        }

        ~WindowSwitcher () {
            if (monitor != null)
                monitor.cancel ();
            wm.get_display ().get_context ().get_backend ().get_monitor_manager ()
             .monitors_changed.disconnect (update_actors);
        }

        void update_background () {
            int width = 0, height = 0;
            wm.get_display ().get_size (out width, out height);

            background.set_size (width, height);
        }

        void update_actors () {
            update_background ();
        }

        void show_background () {
            background.save_easing_state ();
            background.set_easing_duration (250);
            background.set_easing_mode (AnimationMode.EASE_OUT_CUBIC);
            background.opacity = (uint)BACKGROUND_OPACITY;
            background.restore_easing_state ();
        }

        void hide_background () {
            background.save_easing_state ();
            background.set_easing_duration (250);
            background.set_easing_mode (AnimationMode.EASE_OUT_CUBIC);
            background.opacity = 0;
            background.restore_easing_state ();
        }

        bool clicked_icon (Clutter.ButtonEvent event) {
            unowned WindowIcon icon = (WindowIcon)event.get_source ();
            current_window = icon;
            update_indicator_position ();
            dim_windows ();
            return true;
        }

        bool clicked_window (Clutter.ButtonEvent event) {
            unowned SafeWindowClone clone = (SafeWindowClone)event.get_source ();
            foreach (var actor in container.get_children ()) {
                unowned WindowIcon icon = (WindowIcon)actor;
                if (icon.window == clone.window) {
                    current_window = icon;
                    update_indicator_position ();
                    dim_windows ();
                    break;
                }
            }
            return true;
        }

        void window_removed (Actor actor) {
            clone_sort_order.remove (actor);
        }
        public override bool key_press_event (Clutter.KeyEvent event) {
            if (event.keyval == Clutter.Key.Escape) {
                current_window = null;
                close (event.time);
                return true;
            }

            return false;
        }

        public override bool key_release_event (Clutter.KeyEvent event) {
            if ((get_current_modifiers () & modifier_mask) == 0)
                close (event.time);

            return true;
        }

        public override void key_focus_out () {
            close (wm.get_display ().get_current_time ());
        }

        enum SwitcherMode { WINDOWS_P, APPLICATIONS_P, GROUP_P }
        SwitcherMode switcher_mode = WINDOWS_P;

        [CCode (instance_pos = -1)]
        public void handle_switch_windows (Display display, Window? window, Clutter.KeyEvent event,
            KeyBinding binding) {
            set_primary_modifier (binding.get_mask ());
            var now = get_monotonic_time () / 1000;
            if (now - last_switch < MIN_DELTA)
                return;

            // if we were still closing while the next invocation comes in, we need to cleanup
            // things right away
            if (visible && closing) {
                close_cleanup ();
            }

            last_switch = now;

            var workspace = display.get_workspace_manager ().get_active_workspace ();
            var binding_name = binding.get_name ();
            var backward = binding_name.has_suffix ("-backward");
            SwitcherMode old_switcher_mode = switcher_mode;

            // FIXME for unknown reasons, switch-applications-backward won't be emitted, so we
            // test manually if shift is held down
            if (binding_name.has_prefix ("switch-applications")) {
                switcher_mode = APPLICATIONS_P;
                if (!backward)
                    backward = ((get_current_modifiers () & ModifierType.SHIFT_MASK) != 0);
            } else if (binding_name.has_prefix ("switch-group")) {
                switcher_mode = GROUP_P;
                if (!backward)
                    backward = ((get_current_modifiers () & ModifierType.SHIFT_MASK) != 0);
            } else if (binding_name.has_prefix ("switch-windows")) {
                switcher_mode = WINDOWS_P;
                if (!backward)
                    backward = ((get_current_modifiers () & ModifierType.SHIFT_MASK) != 0);
            } else {
                error ("UNKNOWN BINDING");
            }

            prev = null;
            if (old_switcher_mode != switcher_mode)
                close (wm.get_display ().get_current_time (), true);
            else if (visible && !closing) {
                current_window = next_window (workspace, backward);
                update_indicator_position ();
                dim_windows ();
                return;
            }

            if (!collect_windows (workspace))
                return;

            current_window = next_window (workspace, backward);
            update_indicator_position ();

            visible = true;
            closing = false;
            modal_proxy = wm.push_modal (this);

            modal_proxy.set_keybinding_filter ((binding) => {
                // if it's not built-in, we can block it right away
                if (!binding.is_builtin ())
                    return true;

                // otherwise we determine by name if it's meant for us
                var name = binding.get_name ();

                return !(name == "switch-applications" || name == "switch-applications-backward"
                         || name == "switch-windows" || name == "switch-windows-backward"
                         || name == "switch-group" || name == "switch-group-backward");
            });

            show_background ();

            dim_windows ();
            grab_key_focus ();

            if ((get_current_modifiers () & modifier_mask) == 0)
                close (wm.get_display ().get_current_time ());
        }

        void close_cleanup () {
            var display = wm.get_display ();
            var workspace = display.get_workspace_manager ().get_active_workspace ();

            visible = false;
            closing = false;

            window_clones.destroy_all_children ();

            // need to go through all the windows because of hidden dialogs
            unowned GLib.List<Meta.WindowActor> window_actors = display.get_window_actors ();
            foreach (unowned Meta.WindowActor actor in window_actors) {
                if (actor.is_destroyed ())
                    continue;

                unowned Meta.Window window = actor.get_meta_window ();
                if (window.get_workspace () == workspace
                    && window.showing_on_its_workspace ())
                    actor.show ();
            }
        }

        void close (uint time, bool cancel = false) {
            if (closing)
                return;

            closing = true;
            last_switch = 0;

            foreach (var actor in clone_sort_order) {
                unowned SafeWindowClone clone = (SafeWindowClone)actor;

                // current window stays on top
                if (clone.window == current_window.window)
                    continue;

                clone.remove_effect_by_name ("brightness");

                // reset order
                window_clones.set_child_below_sibling (clone, null);

                if (!clone.window.minimized) {
                    clone.save_easing_state ();
                    clone.set_easing_duration (150);
                    clone.set_easing_mode (AnimationMode.EASE_OUT_CUBIC);
                    clone.z_position = 0;
                    clone.opacity = 255;
                    clone.restore_easing_state ();
                }
            }

            if (current_window != null) {
                if (cancel == false)
                    current_window.window.activate (time);
                current_window = null;
            }

            wm.pop_modal (modal_proxy);
            hide_background ();
            close_cleanup ();
        }

        WindowIcon? add_window (Window window) {
            var actor = window.get_compositor_private () as WindowActor;
            if (actor == null)
                return null;

            actor.hide ();

            var clone = new SafeWindowClone (window, true) {
                reactive = true,
                x = actor.x,
                y = actor.y
            };
            clone.button_press_event.connect (clicked_window);

            window_clones.add_child (clone);
            const int ICON_SIZE = 64;

            var icon = new WindowIcon (window, ICON_SIZE, ui_scale_factor) {
                reactive = true,
                opacity = 100,
                x_expand = true,
                y_expand = true,
                x_align = ActorAlign.CENTER,
                y_align = ActorAlign.CENTER
            };
            icon.button_release_event.connect (clicked_icon);
            container.add_child (icon);

            return icon;
        }

        void dim_windows () {
            foreach (var actor in window_clones.get_children ()) {
                unowned SafeWindowClone clone = (SafeWindowClone)actor;

                actor.save_easing_state ();
                actor.set_easing_duration (250);
                actor.set_easing_mode (AnimationMode.EASE_IN_OUT_QUART);

                if (clone.window == current_window.window) {
                    window_clones.set_child_above_sibling (actor, null);
                    actor.remove_effect_by_name ("brightness");
                    actor.z_position = 0;
                } else {
                    if (actor.get_effect ("brightness") == null) {
                        var brightness_effect = new BrightnessContrastEffect ();
                        brightness_effect.set_brightness (DIM_WINDOW_BRIGHTNESS);
                        actor.add_effect_with_name ("brightness", brightness_effect);
                    }

                    actor.z_position = -100;
                }

                actor.restore_easing_state ();
            }
        }

        /**
         * Adds the suitable windows on the given workspace to the switcher
         *
         * @return whether the switcher should actually be started or if there are
         *         not enough windows
         */
        bool collect_windows (Workspace workspace) {
            var display = workspace.get_display ();
            var windows = display.get_tab_list (TabList.NORMAL, workspace);
            var current = display.get_tab_current (TabList.NORMAL, workspace);
            if (prev != null) {
                current = prev;
                prev = null;
            }
            if (windows.length () < 1)
                return false;

            if (windows.length () == 1) {
                var window = windows.data;
                if (window.minimized)
                    window.unminimize ();
                else
                    Clutter.get_default_backend ().get_default_seat ().bell_notify ();

                window.activate (display.get_current_time ());

                return false;
            }

            container.destroy_all_children ();

            string window_class_filter = null;
            Gee.HashSet<string> icons_added = null;

            if (switcher_mode == GROUP_P) {
                window_class_filter = current.get_wm_class ();
            } else if (switcher_mode == APPLICATIONS_P) {
                icons_added = new Gee.HashSet<string> ();
            }

            foreach (var window in windows) {
                string wm_class = null;
                if (switcher_mode != WINDOWS_P)
                    wm_class = window.get_wm_class ();
                if (switcher_mode == GROUP_P) {
                    if (wm_class != window_class_filter)
                        continue;
                } else if (switcher_mode == APPLICATIONS_P) {
                    if (icons_added.contains (wm_class))
                        continue;
                    else
                        icons_added.add (wm_class);
                }
                var clone = add_window (window);
                if (window == current)
                    current_window = clone;
            }

            clone_sort_order = window_clones.get_children ().copy ();

            if (current_window == null) {
                current_window = (WindowIcon)container.get_child_at_index (0);
            }

            // hide the others
            unowned GLib.List<Meta.WindowActor> window_actors = display.get_window_actors ();
            foreach (unowned Meta.WindowActor actor in window_actors) {
                if (actor.is_destroyed ())
                    continue;

                unowned Meta.Window window = actor.get_meta_window ();
                var type = window.window_type;

                if (type != WindowType.DOCK
                    && type != WindowType.DESKTOP
                    && type != WindowType.NOTIFICATION)
                    actor.hide ();
            }
            return true;
        }

        WindowIcon next_window (Workspace workspace, bool backward) {
            Actor actor;
            if (!backward) {
                actor = current_window.get_next_sibling ();
                if (actor == null) {
                    actor = container.get_first_child ();
                }
            } else {
                actor = current_window.get_previous_sibling ();

                if (actor == null) {
                    actor = container.get_last_child ();
                }

            }

            return (WindowIcon)actor;
        }

        /**
         * copied from gnome-shell, finds the primary modifier in the mask and saves it
         * to our modifier_mask field
         *
         * @param mask The modifier mask to extract the primary one from
         */
        void set_primary_modifier (uint mask) {
            if (mask == 0)
                modifier_mask = 0;
            else {
                modifier_mask = 1;
                while (mask > 1) {
                    mask >>= 1;
                    modifier_mask <<= 1;
                }
            }
        }

        Clutter.ModifierType get_current_modifiers () {
            Clutter.ModifierType modifiers;
            double[] axes = {};
            wm.get_display ().get_cursor_tracker ().get_pointer (null, out modifiers);

            return modifiers & Clutter.ModifierType.MODIFIER_MASK;
        }

        private void create_components () {
            // We've already been constructed once, start again
            if (container != null) {
                caption_height = -1.0f;
                destroy_all_children ();
            }

            var layout = new Clutter.FlowLayout (Clutter.FlowOrientation.HORIZONTAL);
            container = new Clutter.Actor ();
            container.layout_manager = layout;
            container.reactive = true;
//            container.button_press_event.connect (container_mouse_press);
//            container.motion_event.connect (container_motion_event);

            Gdk.RGBA rgba = { 255, 255, 255, 1.0 };
            //var rgba = InternalUtils.get_theme_accent_color ();
            Clutter.Color accent_color = {};
            accent_color.init (
                (uint8)(rgba.red * 255),
                (uint8)(rgba.green * 255),
                (uint8)(rgba.blue * 255),
                (uint8)(rgba.alpha * 255)
            );

            int WRAPPER_BORDER_RADIUS = 3;
            string CAPTION_FONT_NAME = "Inter";

            var rect_radius = WRAPPER_BORDER_RADIUS * ui_scale_factor;
            Clutter.Color INDICATOR_BACKGROUND_COLOR = { 250, 250, 250, 150 };
            indicator = new RoundedActor (INDICATOR_BACKGROUND_COLOR, rect_radius);
            indicator.canvas.scale_factor = ui_scale_factor;
            indicator.margin_left = indicator.margin_top =
                indicator.margin_right = indicator.margin_bottom = 0;
            indicator.set_pivot_point (0.5f, 0.5f);

//            var caption_color = "#2e2e31";
            var caption_color = "#d1d131";

            caption = new Clutter.Text.full (CAPTION_FONT_NAME, "", Clutter.Color.from_string (caption_color));
            caption.set_pivot_point (0.5f, 0.5f);
            caption.set_ellipsize (Pango.EllipsizeMode.END);
            caption.set_line_alignment (Pango.Alignment.CENTER);

            add_child (indicator);
            add_child (container);
            add_child (caption);
        }

/*
        bool container_motion_event (Clutter.MotionEvent event) {
            message ("window-switcher container_motion_event");
            var actor = event.stage.get_actor_at_pos (Clutter.PickMode.ALL, (int)event.x, (int)event.y);
            if (actor == null) {
                return true;
            }

            var selected = actor as WindowIcon;
            if (selected == null) {
                return true;
            }

            if (current_window != selected) {
                current_window = selected;
                update_indicator_position ();
            }

            return true;
        }

        bool container_mouse_press (Clutter.ButtonEvent event) {
            message ("window-switcher container_mouse_press");
            if (!closing && event.button == Clutter.Button_PRIMARY) {
                close (event.time);
            }

            return true;
        }
*/

        public bool indicator_opened { get; private set; default = false; }

        void open_indicator () {
            int WRAPPER_PADDING = 12;
            const int MIN_OFFSET = 64;

            if (indicator_opened)
                return;
            container.margin_left = container.margin_top =
                container.margin_right = container.margin_bottom = (WRAPPER_PADDING * 2 * ui_scale_factor);

            var l = container.layout_manager as Clutter.FlowLayout;
            l.column_spacing = l.row_spacing = WRAPPER_PADDING * ui_scale_factor;

            const int ICON_SIZE = 64;
            var indicator_size = (ICON_SIZE + WRAPPER_PADDING * 2) * ui_scale_factor;
            indicator.resize (indicator_size, indicator_size);
//            indicator.set_size (indicator_size, indicator_size);
//            ((Clutter.Canvas) indicator.content).set_size (indicator_size, indicator_size);

            caption.margin_bottom = caption.margin_top = WRAPPER_PADDING * ui_scale_factor;

            var display = wm.get_display ();
            var monitor = display.get_current_monitor ();
            var geom = display.get_monitor_geometry (monitor);

            float container_width;
            container.get_preferred_width (
                ICON_SIZE * ui_scale_factor + container.margin_left + container.margin_right,
                null,
                out container_width
            );

            if (container_width + MIN_OFFSET * ui_scale_factor * 2 > geom.width) {
                container.width = geom.width - MIN_OFFSET * ui_scale_factor * 2;
            }

            float nat_width, nat_height;
            container.get_preferred_size (null, null, out nat_width, null);

            nat_width -= (WRAPPER_PADDING * ui_scale_factor) / container.get_n_children ();

            container.get_preferred_size (null, null, null, out nat_height);

            // For some reason, on Odin, the height of the caption loses
            // its padding after the first time the switcher displays. As a
            // workaround, I store the initial value here once we have it
            // and use that correct value on subsequent attempts.
            if (caption_height == -1.0f) {
                caption_height = caption.height;
            }


            var switcher_height = (int)(nat_height + caption_height / 2 - container.margin_bottom + WRAPPER_PADDING * 3 * ui_scale_factor);
            /*
            message("wrapper.resize ((int)nat_width=%d, switcher_height=%g)",
                    (int)nat_width, switcher_height);
            */

            container.set_size ((int)nat_width, switcher_height);
            container.set_position (
                (int)(geom.x + (geom.width - nat_width) / 2),
                (int)(geom.y + (geom.height - switcher_height) / 2)
            );


            indicator_opened = true;
        }

        void update_indicator_position () {
            const int WRAPPER_PADDING = 12;

            open_indicator ();

            float x, y;
            current_window.allocation.get_origin (out x, out y);
            if (!x.is_finite () || !y.is_finite ()) {
                Idle.add (() => {
                    update_indicator_position ();
                    return Source.REMOVE;
                });
                return;
            }

            indicator.x = container.x +
                //container.margin_left +
                (container.get_n_children () > 1 ? x : 0) - (WRAPPER_PADDING * ui_scale_factor);
            indicator.y = container.y +
                //container.margin_top
                +y - (WRAPPER_PADDING * ui_scale_factor);

            update_caption_text ();
        }

        float caption_height = -1.0f;

        void update_caption_text () {
            const int WRAPPER_PADDING = 12;
            var current_window = current_window.window;
            var current_caption = "n/a";
            if (current_window != null) {
                current_caption = current_window.get_title ();
            }
            caption.set_text (current_caption);
            caption.visible = true;

            // Make caption smaller than the wrapper, so it doesn't overflow.
            caption.width = container.width - WRAPPER_PADDING * 2 * ui_scale_factor;
            caption.set_position (
                container.x +
                WRAPPER_PADDING * ui_scale_factor,
                container.y +
                (int)(container.height - caption_height / 2 - (WRAPPER_PADDING * ui_scale_factor * 2))
            );
            caption.opacity = 255;
        }
    }
}
